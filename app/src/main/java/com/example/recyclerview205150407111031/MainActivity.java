package com.example.recyclerview205150407111031;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMahasiswaListener {

    RecyclerView recyclerView;
    MahasiswaAdapter mahasiswaRecyclerAdapter;
    ArrayList<Mahasiswa> _mahasiswaList;
    Button btnAddData;
    EditText inputNama, inputNIM;
    int[] foto = {R.drawable.ava_male, R.drawable.ava_female};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputNama = findViewById(R.id.input_nama_edit_text);
        inputNIM = findViewById(R.id.input_NIM_edit_text);

        btnAddData = findViewById(R.id.BtnAddData);
        btnAddData.setOnClickListener(this);
        loadData();
        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rvMahasiswa);
        mahasiswaRecyclerAdapter = new MahasiswaAdapter(_mahasiswaList, this, this);
        recyclerView.setAdapter(mahasiswaRecyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void addData(String nama, String NIM) {
        _mahasiswaList.add(new Mahasiswa(nama, NIM, foto[0]));
    }

    private void loadData() {
        _mahasiswaList = new ArrayList<>();
        for(int i = 1;i<10;i++){
            String nama = "Orang "+i;
            String nim = "20515040711100"+i;
            int x = i%2;
            _mahasiswaList.add(new Mahasiswa( nama, nim, foto[x]));
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnAddData.getId())
        {
            String nama = inputNama.getText().toString();
            String NIM = inputNIM.getText().toString();

            if(nama.equals("") || NIM.equals(""))
            {
                Toast.makeText(MainActivity.this,
                        "Nama dan NIM Tidak Boleh Kosong!", Toast.LENGTH_LONG).show();
                return;
            }

            addData(nama, NIM);
            inputNama.setText("");
            inputNIM.setText("");
            mahasiswaRecyclerAdapter.notifyDataSetChanged();
        }
        hideKeyboard((Button)v);
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }catch(Exception ignored) {
        }
    }

    @Override
    public void onMahasiswaClick(int position) {
        Log.d(TAG, "onMahasiswaClick: clicked.");

        Intent intent = new Intent(this, MainActivity2.class);
        intent.putExtra("keyNama", _mahasiswaList.get(position).get_nama());
        intent.putExtra("keyNIM", _mahasiswaList.get(position).get_NIM());
        intent.putExtra("keyFoto", _mahasiswaList.get(position).get_foto());
        startActivity(intent);
    }
}